package main

import "testing"

func TestAdd(t *testing.T) {
	num1 := 1
	num2 := 2
	expectedSum := num1 + num2
	actualSum := add(num1, num2)
	if actualSum != expectedSum {
		t.Error(actualSum, "!=", expectedSum)
	}
}
